/**
 * Created by Aman Panda and Abha Laddha on 1/9/17.
 */

/**
 * Class to represent a Yahtzee Scorer object that keeps track
 * of a player's score and scoring selections.
 */
public class YahtzeeScorer {

    // Instance variables
    private boolean yahtzee, straight, fullHouse, threeKind;
    private int score;

    // Constructor method. Initializes score and booleans keeping
    // track of scoring types.
    public YahtzeeScorer() {
        yahtzee = false;
        straight = false;
        fullHouse = false;
        threeKind = false;
        score = 0;
    }

    /**
     * Method that calls helper methods depending on a dice roll and
     * category that a user selects.
     * Parameters: int[] dice, String category
     * Return: int score (-1 if invalid input)
     */
    public int score(int[] dice, String category){
        if(isValid(dice)) {
            if (category.equalsIgnoreCase("yahtzee"))
                return checkYahtzee(dice, category);
            else if (category.equalsIgnoreCase("straight"))
                return checkStraight(dice, category);
            else if (category.equalsIgnoreCase("fullHouse"))
                return checkFullHouse(dice, category);
            else if (category.equalsIgnoreCase("threeKind"))
                return checkThreeKind(dice, category);
            else
                throw new InvalidDiceException("Invalid score category found!");
        }
        else {
            throw new InvalidDiceException("Invalid dice values found!");
        }
    }

    /**
     * Method that checks that dice input is valid. I.e.
     * the number of dice and the values inputted are valid.
     * Parameters: int[] dice
     * Return: true if valid dice, false if invalid dice
     */
    public boolean isValid(int[] dice)
    {
        if(dice.length==5) {
            int i;
            for (i = 0; i < dice.length; i++)
                if (dice[i] <= 0 || dice[i] > 6)
                    return false;
            return true;
        }
        return false;
    }

    /**
     * Getter method that returns user's current score
     * Returns: int this.score
     */
    public int getCurrentScore(){
        return this.score;
    }

    /**
     * Method that scores a Yahtzee. Deals with insufficient
     * input and the different scoring rules with yahtzee
     * Parameters: int[] dice, String category
     * Return: int amount scored this roll
     */
    public int checkYahtzee(int[] dice, String category) {
        for(int i = 0; i < dice.length-1; i++) {
            if (dice[i] != dice[i + 1])
                return 0;
        }
        if(this.yahtzee==false) {
            score += 50;
            this.yahtzee = true;
            return 50;
        }
        else {
            score+=100;
            return 100;
        }
    }

    /**
     * Method that scores a straight. Deals with insufficient
     * input and the different scoring rules with a straight
     * Parameters: int[] dice, String category
     * Return: int amount scored this roll
     */
    public int checkStraight(int[] dice, String category) {

        if (this.straight == false) {
            int i;
            int[] freqArr = {0, 0, 0, 0, 0, 0};
            for (i = 0; i < dice.length; i++) {
                freqArr[dice[i] - 1]++;
            }
            if (freqArr[0] == 1) {
                for (i = 1; i < dice.length; i++) {
                    if (freqArr[i] != 1) {
                        return 0;
                    }
                }
            }
            if (freqArr[5] == 1) {
                for (i = 1; i < dice.length; i++) {
                    if (freqArr[i] != 1) {
                        return 0;
                    }
                }
            }
            this.straight=true;
            score+=40;
            return 40;
        }
        return 0;
    }

    /**
     * Method that scores a full house. Deals with insufficient
     * input and the different scoring rules with a full house
     * Parameters: int[] dice, String category
     * Return: int amount scored this roll
     */
    public int checkFullHouse(int[] dice, String category) {
        if (fullHouse == false) {
            int i;
            boolean threeOfKind=false, twoOfKind=false;
            int[] freqArr={0,0,0,0,0,0};
            for(i=0;i<dice.length;i++)
                freqArr[dice[i] - 1]++;
            for(i=0;i<dice.length;i++) {
                if (freqArr[i] == 2)
                    twoOfKind=true;
                else if (freqArr[i] == 3)
                    threeOfKind=true;
            }
            if(threeOfKind && twoOfKind) {
                this.fullHouse = true;
                score+=25;
                return 25;
            }
        }
        return 0;
    }

    /**
     * Method that scores a three of a kind. Deals with insufficient
     * input and the different scoring rules with a three of a kind
     * Parameters: int[] dice, String category
     * Return: int amount scored this roll
     */
    public int checkThreeKind(int[] dice, String category) {
        int sum=0;
        if (threeKind == false) {
            int i;
            int[] freqArr={0,0,0,0,0,0};
            for(i=0;i<dice.length;i++) {
                sum += dice[i];
                freqArr[dice[i] - 1]++;
            }
            boolean valid = false;
            for(i=0;i<dice.length;i++) {
                if (freqArr[i] == 3) {
                    valid = true;
                    break;
                }
            }
            if(!valid)
                 sum = 0;
            else {
                score += sum;
                this.threeKind = true;
            }
        }
        return sum;
    }
}
