/**
 * Created by abhaladdha on 1/12/17.
 */
public class InvalidDiceException extends RuntimeException{
        public InvalidDiceException(String message) {
            super(message);
        }
}
