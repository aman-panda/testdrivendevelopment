//Aman Panda and Abha Laddha, TDD, January 13th, 2017

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class test
{
    //Checking for normal Yahtzee
    @Test
    public void checkNormalYahtzee(){
        int[] dice = {6,6,6,6,6};
        YahtzeeScorer ys = new YahtzeeScorer();
        assertEquals(50, ys.score(dice, "yahtzee"));
    }

    //In the case that Yahtzee is already score, the bonus score should be returned
    @Test
    public void checkAlreadyScoredYahtzee(){
        int[] dice = {6,6,6,6,6};
        YahtzeeScorer ys = new YahtzeeScorer();
        ys.score(dice, "yahtzee");
        assertEquals(100, ys.score(dice, "yahtzee"));
    }

    //the dice rolls actually don't form a Yahtzee
    @Test
    public void checkInsufficientYahtzee(){
        int[] dice = {6,1,6,6,6};
        YahtzeeScorer ys = new YahtzeeScorer();
        assertEquals(0, ys.score(dice, "yahtzee"));
    }

    //Checking to see if normal straight works
    @Test
    public void checkNormalStraight(){
        int[] dice = {2,3,4,5,6};
        YahtzeeScorer ys = new YahtzeeScorer();
        assertEquals(40, ys.score(dice, "straight"));
    }

    //In case Straight has already been scored, it shouldn't be scored again
    @Test
    public void checkAlreadyScoredStraight(){
        int[] dice = {2,3,4,5,6};
        YahtzeeScorer ys = new YahtzeeScorer();
        ys.score(dice, "straight");
        assertEquals(0, ys.score(dice, "straight"));
    }

    //The dice rolls don't form a straight
    @Test
    public void checkInsufficientStraight(){
        int[] dice = {2,3,4,1,6};
        YahtzeeScorer ys = new YahtzeeScorer();
        assertEquals(0, ys.score(dice, "straight"));
    }

    //Checking for normal full house
    @Test
    public void checkNormalFullHouse(){
        int[] dice = {2,2,3,3,3};
        YahtzeeScorer ys = new YahtzeeScorer();
        assertEquals(25, ys.score(dice, "fullHouse"));
    }

    //In case Full House has already been scored, it shouldn't be scored again
    @Test
    public void checkAlreadyFullHouse(){
        int[] dice = {2,2,3,3,3};
        YahtzeeScorer ys = new YahtzeeScorer();
        ys.score(dice, "fullHouse");
        assertEquals(0, ys.score(dice, "fullHouse"));
    }

    //In case the dice rolls don't form a full house
    @Test
    public void checkInsufficientFullHouse(){
        int[] dice = {2,2,3,6,3};
        YahtzeeScorer ys = new YahtzeeScorer();
        assertEquals(0, ys.checkYahtzee(dice, "fullHouse"));
    }

    //checking for three of a kind
    @Test
    public void checkNormalThreeKind(){
        int[] dice = {4,4,4,2,1};
        YahtzeeScorer ys = new YahtzeeScorer();
        assertEquals(15, ys.score(dice, "threeKind"));
    }

    //If three of a kind has already been fulfilled, it shouldn't be scored again
    @Test
    public void checkAlreadyThreeKind(){
        int[] dice = {4,4,4,2,1};
        YahtzeeScorer ys = new YahtzeeScorer();
        ys.score(dice, "threeKind");
        assertEquals(0, ys.score(dice, "threeKind"));
    }

    //in case the dice rolls don't form three of a kind
    @Test
    public void checkInsufficientThreeKind(){
        int[] dice = {4,5,4,2,1};
        YahtzeeScorer ys = new YahtzeeScorer();
        assertEquals(0, ys.score(dice, "threeKind"));
    }

    //Ensure the scoring is accurate
    @Test
    public void checkScore1(){
        YahtzeeScorer ys = new YahtzeeScorer();
        int[] dice2={1,1,1,2,2};
        ys.score(dice2, "fullHouse");
        int[] dice3={1,2,3,4,5};
        ys.score(dice3, "straight");
        int[] dice4={1,1,1,1,1};
        ys.score(dice4, "yahtzee");
        ys.score(dice4, "yahtzee");
        assertEquals(215,ys.getCurrentScore());
    }

    //Ensure scoring is accurate with different categories
    @Test
    public void checkScore2(){
        int[] dice = {1,1,1,4,5};
        YahtzeeScorer ys = new YahtzeeScorer();
        ys.score(dice, "threeKind");
        int[] dice2={1,1,1,2,2};
        ys.score(dice2, "fullHouse");
        int[] dice3={1,2,3,4,5};
        ys.score(dice3, "straight");
        assertEquals(77,ys.getCurrentScore());
    }

    //Ensure scoring is accurate with different categories
    @Test
    public void checkScore3(){
        int[] dice = {1,1,1,4,5};
        YahtzeeScorer ys = new YahtzeeScorer();
        ys.score(dice, "threeKind");
        int[] dice2={1,1,1,2,2};
        ys.score(dice2, "fullHouse");
        int[] dice3={1,2,3,4,5};
        ys.score(dice3, "straight");
        int[] dice4={1,1,1,1,1};
        ys.score(dice4, "yahtzee");
        ys.score(dice4, "yahtzee");
        assertEquals(227,ys.getCurrentScore());
    }

    //In case we get dice values less than equal to 0 or greater than 6
    @Test
    public void testInvalidDiceValues() {
        try {
            YahtzeeScorer ys = new YahtzeeScorer();
            int[] dice={3,4,7,8,9};
            if (ys.score(dice, "yahtzee") >=0)
                fail("Invalid values found!");
        }
        catch (InvalidDiceException success){

        }
    }

    //In case we input an invalid category
    @Test
    public void testInvalidScoringCategory() {
        try {
            YahtzeeScorer ys = new YahtzeeScorer();
            int[] dice={1,1,1,1,1};
            if (ys.score(dice, "panda") >=0)
                fail("Invalid category!");
        }
        catch (InvalidDiceException success){
        }
    }
}
